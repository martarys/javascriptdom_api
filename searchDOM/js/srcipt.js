//pobieramy z html obiekt container 
var container = document.getElementById("container");

//pobieranie po nazwie
var firstName = document.getElementsByName("firstName")[0];

//pobieranie po nazwie tagu
var paragraphs = document.getElementsByTagName("p");

//pobieranie po nazwie class
var texts = document.getElementsByClassName("text");

// querySelector i querySelectorAll
var h1 = document.querySelector("h1");

var form = document.querySelector("#myForm");

var submit = form.querySelector("button[type='submit']");

var links = document.querySelectorAll("a");

var firstForm = document.forms[0];

