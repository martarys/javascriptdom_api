var list = document.querySelector("#list");

//podgląd wszystkich elementów dzieci listy
var children = list.childNodes;

var elementChildren = list.children;

//inny sposób na odwołanie się do pierwszego elementu
var first = list.firstChild;
var firstElement = list.firstElementChild;

var second = firstElement.nextSibling;
var secondElement = firstElement.nextElementSibling;

var parent = second.parentNode;

secondElement.parentNode.removeChild(secondElement);